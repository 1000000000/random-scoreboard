module Main exposing (main)

import Browser
import Dict exposing (Dict)
import File.Download as Download
import Html
import Html.Events as HtmlEvents
import Json.Encode as E
import Zip exposing (zip)

init : a -> ( (), Cmd {} )
init = always ( (), Cmd.none )

view : () -> Browser.Document {}
view () =
  { title = "Zip Download Test"
  , body =
    [ Html.div
      []
      [ Html.text "Empty Minecraft Data Pack"
      ]
    , Html.button
      [ HtmlEvents.onClick {}
      ]
      [ Html.text "download"
      ]
    ]
  }

testFile : Dict String Zip.File
testFile =
  [ ( "pack_format"
    , E.int 6
    )
  , ( "description"
    , E.string "A data pack that does nothing!"
    )
  ]
  |> E.object
  |> Tuple.pair "pack"
  |> List.singleton
  |> E.object
  |> E.encode 4
  |> Zip.RegularFile
  |> Dict.singleton "pack.mcmeta"

update : {} -> () -> ( (), Cmd {} )
update {} _ =
  ( ()
  , testFile
  |> zip
  |> Download.bytes "test.zip" "application/zip"
  )

subscriptions : () -> Sub {}
subscriptions = always Sub.none

main : Program () () {}
main =
  Browser.document
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }
