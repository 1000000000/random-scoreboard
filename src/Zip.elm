module Zip exposing
  ( zip
  , File(..)
  )

import Array exposing (Array)
import Bitwise
import Bytes exposing
  ( Bytes
  , Endianness (..)
  )
import Bytes.Decode as D
import Bytes.Encode as E
import Dict exposing (Dict)

type File
  = RegularFile String
  | Directory (Dict String File)

stringToBytes : String -> List Int
stringToBytes =
  let
    codeToUtf8 : Int -> List Int
    codeToUtf8 c =
      if
        c < 0x80
      then
        [Bitwise.and 0x7F c]
      else
        ( if
            c < 0x800
          then
            [ c
            |> Bitwise.shiftRightZfBy 6
            |> Bitwise.and 0x1F
            |> Bitwise.or 0xC0
            ]
          else
            ( if
                c < 0x10000
              then
                [ c
                |> Bitwise.shiftRightZfBy 12
                |> Bitwise.and 0xF
                |> Bitwise.or 0xE0
                ]
              else
                [ c
                |> Bitwise.shiftRightZfBy 18
                |> Bitwise.and 0x9
                |> Bitwise.or 0xF0
                , c
                |> Bitwise.shiftRightZfBy 12
                |> Bitwise.and 0x3F
                |> Bitwise.or 0x80
                ]
            )
            ++
            [ c
            |> Bitwise.shiftRightZfBy 6
            |> Bitwise.and 0x3F
            |> Bitwise.or 0x80
            ]
        )
        ++
        [ c
        |> Bitwise.and 0x3F
        |> Bitwise.or 0x80
        ]
  in
    String.toList >> List.concatMap (Char.toCode >> codeToUtf8)

crcGenerator : Int
crcGenerator = 0xedb88320

calculateByteCrc : Int -> Int
calculateByteCrc x =
  let
    crcStep : Int -> Int
    crcStep y =
      y
      |> Bitwise.shiftRightZfBy 1
      |> if
          Bitwise.and y 0x1 == 1
        then
          Bitwise.xor crcGenerator
        else
          identity
  in
    crcStep
    |> List.repeat 8
    |> List.foldl (<|) x

crcTable : Array Int
crcTable =
  Array.initialize 256 calculateByteCrc

lookupCrc : Int -> Int
lookupCrc x =
  case Array.get x crcTable of
    Just result -> result
    Nothing -> calculateByteCrc x

crc32 : String -> Int
crc32 =
  let
    crcByte : Int -> Int -> Int
    crcByte byte crc =
      byte
      |> Bitwise.xor crc
      |> Bitwise.and 0xFF
      |> lookupCrc
      |> Bitwise.xor (Bitwise.shiftRightZfBy 8 crc)
      in
    stringToBytes >> List.foldl crcByte (Bitwise.complement 0) >> Bitwise.complement

localFileHeaderAndContents : String -> String -> E.Encoder
localFileHeaderAndContents name contents =
  E.sequence
    [ E.unsignedInt32 LE 0x04034b50 -- local file header signature
    , E.unsignedInt16 LE 0x000a     -- version needed to extract (1.0)
    , E.unsignedInt16 LE 0x0000     -- general purpose bit flag (none)
    , E.unsignedInt16 LE 0x0000     -- compression method (none)
    , E.unsignedInt16 LE 0x0000     -- last mod file time
    , E.unsignedInt16 LE 0x0000     -- last mod file date
    , E.unsignedInt32 LE            -- crc-32
      <| crc32 contents
    , E.unsignedInt32 LE            -- compressed size
      <| E.getStringWidth contents
    , E.unsignedInt32 LE            -- uncompressed size
      <| E.getStringWidth contents
    , E.unsignedInt16 LE            -- filename length
      <| E.getStringWidth name
    , E.unsignedInt16 LE 0x0000     -- extra field length (0)
    , E.string name                 -- filename
    , E.string contents             -- file data
    ]

localEntryWidth : String -> String -> Int
localEntryWidth name contents =
  30
  + E.getStringWidth name
  + E.getStringWidth contents

-- some notes about internal and external file attributes:
-- directories seem to have internal file attribute 0x0010 while text files have 0x0000
-- file permissions are stored in the external file attribute
centralDirectoryFileHeader : String -> String -> Int -> E.Encoder
centralDirectoryFileHeader name contents localHeaderOffset =
  E.sequence
    [ E.unsignedInt32 LE 0x02014b50 -- local file header signature
    , E.unsignedInt16 LE 0x0314     -- version made by (unix, 2.0)
    , E.unsignedInt16 LE 0x000a     -- version needed to extract (1.0)
    , E.unsignedInt16 LE 0x0000     -- general purpose bit flag (none)
    , E.unsignedInt16 LE 0x0000     -- compression method (none)
    , E.unsignedInt16 LE 0x0000     -- last mod file time
    , E.unsignedInt16 LE 0x0000     -- last mod file date
    , E.unsignedInt32 LE            -- crc-32
      <| crc32 contents
    , E.unsignedInt32 LE            -- compressed size
      <| E.getStringWidth contents
    , E.unsignedInt32 LE            -- uncompressed size
      <| E.getStringWidth contents
    , E.unsignedInt16 LE            -- filename length
      <| E.getStringWidth name
    , E.unsignedInt16 LE 0x0000     -- extra field length (0)
    , E.unsignedInt16 LE 0x0000     -- file comment length (0)
    , E.unsignedInt16 LE 0x0000     -- disk number start (0)
    , E.unsignedInt16 LE 0x0000     -- internal file attributes (binary file)
    , E.unsignedInt32 LE 0x00000000 -- external file attributes (host dependent, hopefully unneeded)
    , E.unsignedInt32 LE            -- relative offset of local header
      <| localHeaderOffset
    , E.string name                 -- filename
    ]

centralDirHeaderWidth : String -> Int
centralDirHeaderWidth name =
  46
  + E.getStringWidth name

type alias ZipData =
  { localHeaders : E.Encoder
  , centralDirHeaders : E.Encoder
  , localHeadersWidth : Int
  , centralDirWidth : Int
  , entryCount : Int
  }

files : String -> File -> ZipData -> ZipData
files filename file prevData =
  case file of
    RegularFile contents ->
      { localHeaders =
        E.sequence
          [ prevData.localHeaders
          , localFileHeaderAndContents filename contents
          ]
      , centralDirHeaders =
        E.sequence
          [ prevData.centralDirHeaders
          , centralDirectoryFileHeader filename contents prevData.localHeadersWidth
          ]
      , localHeadersWidth =
        prevData.localHeadersWidth
        + localEntryWidth filename contents
      , centralDirWidth =
        prevData.centralDirWidth
        + centralDirHeaderWidth filename
      , entryCount = prevData.entryCount + 1
      }
    Directory contents ->
      let
        dataWithDir : ZipData
        dataWithDir =
          { localHeaders =
            E.sequence
              [ prevData.localHeaders
              , localFileHeaderAndContents (filename ++ "/") ""
              ]
          , centralDirHeaders =
            E.sequence
              [ prevData.centralDirHeaders
              , centralDirectoryFileHeader (filename ++ "/") "" prevData.localHeadersWidth
              ]
          , localHeadersWidth =
            prevData.localHeadersWidth
            + localEntryWidth (filename ++ "/") ""
          , centralDirWidth =
            prevData.centralDirWidth
            + centralDirHeaderWidth (filename ++ "/")
          , entryCount = prevData.entryCount + 1
          }
        in
          Dict.foldl (String.append (filename ++ "/") >> files) dataWithDir contents

zip : Dict String File -> Bytes
zip rootFiles =
  let
    initialData : ZipData
    initialData =
      { localHeaders = E.sequence []
      , centralDirHeaders = E.sequence []
      , localHeadersWidth = 0
      , centralDirWidth = 0
      , entryCount = 0
      }
    
    finishedData : ZipData
    finishedData = Dict.foldl files initialData rootFiles
  in
    [ finishedData.localHeaders
    , finishedData.centralDirHeaders
    , E.unsignedInt32 LE 0x06054b50 -- end of central dir signature
    , E.unsignedInt16 LE 0x0000     -- number of this disk
    , E.unsignedInt16 LE 0x0000     -- number of the disk with the start of the central directory
    , E.unsignedInt16 LE            -- total number of entries in the central dir on this disk
      <| finishedData.entryCount
    , E.unsignedInt16 LE            -- total number of entries in the central dir
      <| finishedData.entryCount
    , E.unsignedInt32 LE            -- size of the central directory
      <| finishedData.centralDirWidth
    , E.unsignedInt32 LE            -- offset of start of central directory with respect to the starting disk number
      <| finishedData.localHeadersWidth
    , E.unsignedInt16 LE 0x0000     -- zipfile comment length
    ]
    |> E.sequence
    |> E.encode
