# Random Scoreboard Challenge

A webapp that generates a random Minecraft datapacks to run challenges where player compete
to maximize their score based on a randomized and secret formula of stats.

Check it out [here](https://1000000000.gitlab.io/random-scoreboard) (Hosted on Gitlab pages)
