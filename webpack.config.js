; const path = require('path')
; const webpack = require('webpack')

; module.exports =
  { module:
    { rules:
      [ { test: /\.elm$/
        , exclude:
          [ /elm-stuff/
          , /node_modules/
          ]
        , use:
          { loader: 'elm-webpack-loader'
          , options:
            { cwd: __dirname
            }
          }
        }
      ]
    }
  , entry: "./js/index.js"
  , output:
    { filename: "main.js"
    , path: path.resolve(__dirname, "dist")
    }
  , devServer:
    { hot: false
    , port: 8000
    , contentBase: path.resolve(__dirname, "dist")
    }
  }
